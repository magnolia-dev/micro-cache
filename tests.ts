import * as assert from 'assert';
import { cache } from './lib';

// TESTS

const c1 = cache({
  lifespan: 10,
});

assert.deepEqual(
  c1.set('a', 100),
  100,
);

assert.deepEqual(
  c1.get('a'),
  100,
);

assert.equal(
  c1.get('b'),
  undefined,
);

assert(!c1.keyHasExpired('b'));

setTimeout(() => {
  assert(c1.keyHasExpired('a')); 
}, 10);

setTimeout(() => {
  assert.deepEqual(c1.expiredKeys(), ['a']);
}, 10);

setTimeout(() => {
  c1.set('a', true);
  assert(!c1.keyHasExpired('a')); 
}, 20);

const c2 = cache({
  lifespan: 10000,
});

c2.set('b', { data: 'hello' });

assert.deepEqual(
  c2.get('b'),
  { data: 'hello' },
);

setTimeout(() => {
  assert(!c2.keyHasExpired('b')); 
}, 100);

const c3 = cache();

c3.set(3, [1,2,3]);
assert.equal(c3.length(), 1);
c3.remove(3);
assert.equal(c3.length(), 0);

