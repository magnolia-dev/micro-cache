interface CacheOptions {
    lifespan?: number;
}
export declare function cache(explicitOptions?: CacheOptions): {
    set(key: any, value: any): any;
    get(key: any): any;
    keyHasExpired(key: any): Boolean;
    expiredKeys(): any[];
    remove(key: any): boolean;
    length(): number;
};
export {};
