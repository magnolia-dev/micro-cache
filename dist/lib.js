"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
function cache(explicitOptions) {
    if (explicitOptions === void 0) { explicitOptions = {}; }
    var defaultOptions = {
        lifespan: 24 * 60 * 60 * 1000,
    };
    var options = __assign(__assign({}, defaultOptions), explicitOptions);
    var map = new Map();
    return {
        set: function (key, value) {
            var expirationDate = milliseconds(options.lifespan);
            var entry = { value: value, expirationDate: expirationDate };
            map.set(key, entry);
            return this.get(key);
        },
        get: function (key) {
            var _a;
            var value = ((_a = map.get(key)) !== null && _a !== void 0 ? _a : {}).value;
            return value;
        },
        keyHasExpired: function (key) {
            var _a;
            var expirationDate = ((_a = map.get(key)) !== null && _a !== void 0 ? _a : {}).expirationDate;
            return expirationDate < new Date();
        },
        expiredKeys: function () {
            return Array.from(map.keys()).filter(this.keyHasExpired);
        },
        remove: function (key) {
            return map.delete(key);
        },
        length: function () {
            return map.size;
        }
    };
}
exports.cache = cache;
// Create a date milliseconds in the future
function milliseconds(ms) {
    var dt = new Date();
    dt.setSeconds(dt.getSeconds() + ms / 1000);
    return dt;
}
