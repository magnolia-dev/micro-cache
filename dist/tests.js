"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var lib_1 = require("./lib");
// TESTS
var c1 = lib_1.cache({
    lifespan: 10,
});
assert.deepEqual(c1.set('a', 100), 100);
assert.deepEqual(c1.get('a'), 100);
assert.equal(c1.get('b'), undefined);
assert(!c1.keyHasExpired('b'));
setTimeout(function () {
    assert(c1.keyHasExpired('a'));
}, 10);
setTimeout(function () {
    assert.deepEqual(c1.expiredKeys(), ['a']);
}, 10);
setTimeout(function () {
    c1.set('a', true);
    assert(!c1.keyHasExpired('a'));
}, 20);
var c2 = lib_1.cache({
    lifespan: 10000,
});
c2.set('b', { data: 'hello' });
assert.deepEqual(c2.get('b'), { data: 'hello' });
setTimeout(function () {
    assert(!c2.keyHasExpired('b'));
}, 100);
var c3 = lib_1.cache();
c3.set(3, [1, 2, 3]);
assert.equal(c3.length(), 1);
c3.remove(3);
assert.equal(c3.length(), 0);
