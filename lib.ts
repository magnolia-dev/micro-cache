interface CacheOptions {
  lifespan?: number,
}
interface Cache {
  get: (key:any) => CacheEntry,
}
interface CacheEntry {
  value: any,
  expirationDate: Date,
}

export function cache(explicitOptions: CacheOptions = {}) {
  const defaultOptions = {
    lifespan: 24 * 60 * 60 * 1000,
  }

  const options = { ...defaultOptions, ...explicitOptions };
  const map = new Map(); 

  return {
    set(key:any, value:any): any {
      const expirationDate = milliseconds(options.lifespan);
      const entry = { value, expirationDate };
      map.set(key, entry);
      return this.get(key);
    },
    get(key:any): any {
      const { value } = map.get(key) ?? {};
      return value;
    },
    keyHasExpired(key:any): Boolean {
      const { expirationDate } = map.get(key) ?? {};
      return expirationDate < new Date();
    },
    expiredKeys(): any[] {
      return Array.from(map.keys()).filter(this.keyHasExpired);
    },
    remove(key:any): boolean {
      return map.delete(key);
    },
    length(): number {
      return map.size;
    }
  }
}

// Create a date milliseconds in the future
function milliseconds(ms:number): Date {
  let dt = new Date();
  dt.setSeconds(dt.getSeconds() + ms / 1000);
  return dt;
}

